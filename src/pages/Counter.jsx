import React, { useState } from "react";

import "./Counter.css";
import CounterDetail from "../components/CounterDetail/CounterDetail.jsx";
import CounterButton from "../components/CounterButton/CounterButton.jsx";

const Counter = () => {
  const [counter, setCounter] = useState(0);

  const handleDecreaseDisabled = (counter) => {
    if (counter <= 0) {
      return true;
    } else {
      return false;
    }
  };

  const handleResetDisabled = (counter) => {
    if (counter == 0) {
      return true;
    }
  };

  return (
    <div class="main-counter">
      <CounterDetail counter={counter} />
      <CounterButton
        title="increase"
        onCounterClick={() => setCounter(counter + 1)}
      />

      <CounterButton
        title="decrease"
        onCounterClick={() => setCounter(counter - 1)}
        disabled={handleDecreaseDisabled(counter)}
      />

      <CounterButton
        title="reset"
        onCounterClick={() => setCounter(0)}
        disabled={handleResetDisabled(counter)}
      />
    </div>
  );
};
export default Counter;
