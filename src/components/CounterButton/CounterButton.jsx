import React from "react";

import "./CounterButton.css";
const CounterButton = ({ onCounterClick, title, disabled }) => {
  //props
  return (
    <button class="button" onClick={onCounterClick} disabled={disabled}>
      {title}
    </button>
  );
  //props.onIncrease   //props.title //props.disabled
};
export default CounterButton;
