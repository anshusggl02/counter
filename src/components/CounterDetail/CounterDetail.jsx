import React from "react";

import "./CounterDetail.css";

const CounterDetail = ({ counter }) => {
  //props
  return <h1 class="counter-value">{counter}</h1>; //props.counter
};
export default CounterDetail;
