import React from "react";

import Counter from "../pages/Counter.jsx";

function App() {
  return (
    <div className="App">
      <Counter />
    </div>
  );
}

export default App;
